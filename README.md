## Instagram

Project thực hiện một mạng xã hội chia sẻ ảnh đơn giản

# Các chức năng của website:
  * Đăng nhập/Đăng ký
  * Đăng bài (có thể đăng nhiều ảnh)
  * Like bài đăng và comment bài đăng
  * Bookmark bài đăng
  * Tìm kiếm user
  * CRUD thông tin tài khoản
---
# Các kỹ thuật sử dụng:
  * Auto suggestion (Jquery auto complete)
  * Dialog modal (JQuery dialog)
  * Ajax
  * Deploy lên heroku
  * Lưu trữ ảnh trên cloud
---
# Hướng dẫn sử dụng:
  1. Install Ruby, Rails.
  2. Clone repo về, mở terminal, cd vào thư mực chứa project và thực hiện các lệnh sau:
  3. `bundle install`
  4. `rake db:migrate`
  5. `rails s`
